const fs = require("fs");
var clickJson = require("./clicks.json");

function findIpAboveTenClicks() {
  return clickJson.reduce((acc, click) => {
    acc[click.ip] = acc[click.ip] ? acc[click.ip] + 1 : 1;
    return acc;
  }, {});
}

function findExpensiveClicks() {
  let ipAboveTenClicks = findIpAboveTenClicks(clickJson);

  return clickJson
    .filter(click => (ipAboveTenClicks[click.ip] > 10 ? false : true))
    .reduce((acc, click) => {
      if (
        clickJson.findIndex(
          ele =>
            new Date(ele.timestamp).getHours() ===
              new Date(click.timestamp).getHours() &&
            ele.ip === click.ip &&
            ele.amount > click.amount
        ) < 0 &&
        acc.findIndex(
          element =>
            new Date(element.timestamp).getHours() ===
              new Date(click.timestamp).getHours() &&
            element.ip === click.ip &&
            element.amount === click.amount
        ) < 0
      ) {
        acc.push(click);
      }

      return acc;
    }, []);
}

function writeToFile(content) {
  fs.writeFile("./resultset.json", JSON.stringify(content), function(err) {
    if (err) {
      return console.log(err);
    }

    console.log("A JSON file with resultset has been generated.");
  });
}

writeToFile(findExpensiveClicks());
